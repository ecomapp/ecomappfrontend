import React from "react";

function PlatformManager() {
  return (
    <div>
      <header>
        <nav>
          <ul>
            <li><a href="/">Home</a></li>
            <li><a href="/platform-manager/products">Products</a></li>
            <li><a href="/platform-manager/add-product">Add Product</a></li>
            <li><a href="/platform-manager/orders">Orders</a></li>
          </ul>
        </nav>
      </header>
      <main>
        {/* Platform manager components go here */}
      </main>
      <footer>
        {/* Footer content goes here */}
      </footer>
    </div>
  );
}

export default PlatformManager;