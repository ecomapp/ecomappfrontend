import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import Login from './Login';
import ProductOwner from './ProductOwner';        //may be writing components/Customer will be needed in the future
import Customer from './Customer';
import PlatformManager from './PlatformManager';

function App() 
{
  return (
    <Router>
      <Switch>
        <Route exact path="/" component={Login} />
        <Route path="/login" component={Login} />     
        <Route path="/product-owner" component={ProductOwner} />
        <Route path="/customer" component={Customer} />
        <Route path="/platform-manager" component={PlatformManager} />
      </Switch>
    </Router>
  );
}

function PrivateRoute({ component: Component, ...rest }) 
{
  // Check if user is authenticated, if not redirect to login page
  const isAuthenticated = true; // replace with your authentication logic
  return (
    <Route {...rest} render={(props) => (
      isAuthenticated
        ? <Component {...props} />
        : <Redirect to="/" />
    )} />
  );
}

function Login() 
{
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const history = useHistory();

  const handleLogin = (e) => {
    e.preventDefault();
    // TODO: Send login request to the server with email and password
    // For successful login
    const user = {
      id: 1,
      name: 'John Doe',
      role: 'product_owner' // or 'customer'
    };
    localStorage.setItem('user', JSON.stringify(user));
    history.push(`/${user.role}`); 
  };

  return (
    <div>
      <h1>Login</h1>
      <form onSubmit={handleSubmit}>
        <label>
          Email:
          <input type="email" value={email} onChange={(e) => setEmail(e.target.value)} />
        </label>
        <label>
          Password:
          <input type="password" value={password} onChange={(e) => setPassword(e.target.value)} />
        </label>
        <button type="submit">Login</button>
      </form>
    </div>
  );
}

export default App;