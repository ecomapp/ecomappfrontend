import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';

function Login() 
{
     const [email, setEmail] = useState('');
     const [password, setPassword] = useState('');
     const history = useHistory();

     const handleEmailChange = (event) => {
     setEmail(event.target.value);
  };

    const handlePasswordChange = (event) => {
    setPassword(event.target.value);
  };

    const handleSubmit = async (event) => {
    event.preventDefault();
  
    try {
      // Send a POST request to the server to log in the user
      const response = await fetch('/api/login', {
          method: 'POST',
          headers: { 'Content-Type': 'application/json' },
          body: JSON.stringify({ email, password }),
      });
  
      if (response.ok) 
      {
        // Redirect user to platform manager page if login is successful
          history.push('/platform-manager');
      } 

      else 
      {
        // Display error message if login is unsuccessful
          const errorData = await response.json();
          setError(errorData.message);
      }

    } 
    
    catch (error)
    {
        console.error('Login error:', error);
        setError('An error occurred while logging in. Please try again later.');
    }
  };

  return (
    <div>
      <h1>Login</h1>
      <form onSubmit={handleSubmit}>
        <label>
          Email:
          <input type="email" value={email} onChange={handleEmailChange} />
        </label>
        <br />
        <label>
          Password:
          <input type="password" value={password} onChange={handlePasswordChange} />
        </label>
        <br />
        <button type="submit">Login</button>
      </form>
    </div>
  );
}

export default Login;