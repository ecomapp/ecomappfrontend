import React from 'react';

function Customer() {
  return (
    <div>
      <h1>Customer</h1>
      {/* Add your customer page content here */}
      <div className="products">
        <div className="product">
          <img src="https://via.placeholder.com/150x150" alt="Product 1" />
          <h2>Product 1</h2>
          <p>Description of Product 1</p>
          <button>Add to cart</button>
        </div>
        <div className="product">
          <img src="https://via.placeholder.com/150x150" alt="Product 2" />            
          <h2>Product 2</h2>
          <p>Description of Product 2</p>
          <button>Add to cart</button>
        </div>
        <div className="product">
          <img src="https://via.placeholder.com/150x150" alt="Product 3" />
          <h2>Product 3</h2>
          <p>Description of Product 3</p>
          <button>Add to cart</button>
        </div>
      </div>
    </div>
  );
}

export default Customer;