import React from 'react';

function ProductOwner() {
  return (
    <div>
      <h1>Product Owner Dashboard</h1>
      <div>
        <h2>Products</h2>
        {/* Product list goes here */}
      </div>
      <div>
        <h2>Orders</h2>
        {/* Order list goes here */}
      </div>
      <div>
        <h2>Customers</h2>
        {/* Customer list goes here */}
      </div>
    </div>
  );
}

export default ProductOwner;